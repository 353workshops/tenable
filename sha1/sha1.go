package main

import (
	"compress/gzip"
	"crypto/sha1"
	"fmt"
	"io"
	"os"
	"strings"
)

/*
Usages for interfaces:
- Polymorphism
- Change how runtime works with your types
- Mocking

Interfaces say what we need, not what we provide.
- Interface are small (rule of thumb <= 5)
- Return types, accept interfaces
*/

/* Thought experiment: sorting

type Sortable interface {
	Less(i, j int) bool
	Swap(i, j int)
	Len() int
}

func Sort(s Sortable) {}

type Reader interface {
	Read([]byte) (int, error)
}

type Reader interface {
	Read(int) ([]byte, error)
}
*/

func main() {
	fmt.Println(fileSHA1("httpd.log.gz"))
	fmt.Println(fileSHA1("sha1.go"))
}

// $ cat httpd.log.gz| gunzip | sha1sum
// Exercise: Gunzip only if file name ends with .gz
// $ cat sha1.go | sha1sum
func fileSHA1(fileName string) (string, error) {
	// cat httpd.log.gz
	file, err := os.Open(fileName)
	if err != nil {
		return "", err
	}
	defer file.Close() // TODO: Check for error?
	var r io.Reader = file
	// r := io.Reader(file)

	if strings.HasSuffix(fileName, ".gz") {
		// | gunzip
		// r, err := gzip.NewReader(file) // BUG: r's scope is only the if
		r, err = gzip.NewReader(file)
		if err != nil {
			return "", fmt.Errorf("%q: bad gzip: %w", fileName, err)
		}
		// fmt.Printf("r: %p\n", r)
	}

	// | sha1sum
	w := sha1.New()

	if _, err := io.Copy(w, r); err != nil {
		return "", fmt.Errorf("can't copy: %w", err)
	}

	sig := w.Sum(nil)
	return fmt.Sprintf("%x", sig), nil
}
