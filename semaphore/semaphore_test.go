package semaphore

import (
	"math/rand/v2"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestSemaphone(t *testing.T) {
	const size = 3
	s, err := New(size)
	require.NoError(t, err)

	maxCount := int64(0)
	ch := make(chan int64, 1000)

	go func() {
		for {
			v := <-ch
			if v > maxCount {
				maxCount = v
			}
		}
	}()

	count := int64(0)
	var wg sync.WaitGroup

	for range 7 {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for range 100 {
				s.Enter()
				v := atomic.AddInt64(&count, 1)
				ch <- v
				n := time.Duration(rand.IntN(100))
				time.Sleep(n)
				atomic.AddInt64(&count, -1)
				s.Leave()
			}
		}()
	}
	wg.Wait()
	require.LessOrEqual(t, maxCount, int64(size))
}
