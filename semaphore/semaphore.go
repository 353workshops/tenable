package semaphore

import (
	"fmt"
)

// Semaphore is a channel based semaphore.
type Semaphore chan struct{}

// New returns semaphore of size `size`.
func New(size int) (Semaphore, error) {
	if size <= 0 {
		return nil, fmt.Errorf("size (%d) <= 0", size)
	}

	s := make(Semaphore, size)
	return s, nil
}

// Enter enters the semaphore.
func (s Semaphore) Enter() {
	s <- struct{}{}
}

// Leave leaves the semaphore.
func (s Semaphore) Leave() {
	<-s
}
