package user

import (
	"fmt"
	"testing"
)

var (
	sdb        SliceCache
	mdb        MapCache
	n          = 1000 // When n was 10, slice was faster
	benchLogin = "no such user"
)

func init() {
	mdb = make(MapCache)
	for i := 0; i < n; i++ {
		u := User{Login: fmt.Sprintf("u-%04d", i)}
		sdb = append(sdb, u)
		mdb[u.Login] = u
	}
}

func BenchmarkSlice(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, ok := sdb.Find(benchLogin)
		if ok {
			b.Fatal("found non-existing login")
		}
	}
}

func BenchmarkMap(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, ok := mdb.Find(benchLogin)
		if ok {
			b.Fatal("found non-existing login")
		}
	}
}

// In the terminal
// go test -run NONE -bench . -count 3
// See "go help testflag"

/* Design for performance

Go implementation
type Reader interface {
	Read(p []byte) (n int, err error)
}

Python Implementation
type Reader interface {
	Read(n int) (data []byte, err error)
}

The Python implement implementation *must* allocate a new slice on every read.
In the Go version, the user can decided to re-use the same slice.
*/
