package main

import (
	"bytes"
	"fmt"
	"log"
	"os"
)

func main() {
	data, err := os.ReadFile("yankee.txt")
	if err != nil {
		log.Fatalf("error: %s", err)
	}

	author, err := findAuthor(data)
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	fmt.Printf("len: %d, cap: %d\n", len(author), cap(author))
	fmt.Println(string(author))
}

func findAuthor(text []byte) ([]byte, error) {
	author := []byte("Author: ")
	i := bytes.Index(text, author)
	if i == -1 {
		return nil, fmt.Errorf("can't find author")
	}

	i += len(author) // Skip "Author: "
	j := bytes.IndexByte(text[i:], '\n')
	if j == -1 {
		return nil, fmt.Errorf("can't find author")
	}

	// return text[i : i+j], nil
	// We don't want to keep reference to text here.
	out := make([]byte, j)
	copy(out, text[i:i+j])
	return out, nil
}
