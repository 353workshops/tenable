# Go Workshop @ Tenable

Miki Tebeka
📬 [miki@ardanlabs.com](mailto:miki@ardanlabs.com), 𝕏 [@tebeka](https://twitter.com/tebeka), 👨 [mikitebeka](https://www.linkedin.com/in/mikitebeka/), ✒️[blog](https://www.ardanlabs.com/blog/)

#### Shameless Plugs

- [LinkedIn Learning Classes](https://www.linkedin.com/learning/instructors/miki-tebeka)
- [Books](https://pragprog.com/search/?q=miki+tebeka)

## Session 2: Interfaces, Concurrency & Code Generation

### Agenda

- A look at interfaces in the standard library
- Getting started with generics
- Writing concurrent code the go-ish way
- Go tooling - writing code generators and tools in Go for Go code

### Code

- [gen_db.go](catalog/gen_db.go) - Generating code
- [semaphore.go](semaphore/semaphore.go) - A buffered channel based semaphore
- [counter.go](counter/counter.go) - The race detector, `sync.Mutex`
- [go_chan.go](go_chan/go_chan.go) - Concurrency
- [stack.go](stack/stack.go) - Generic containers
- [ml.go](ml/ml.go) - Generic functions
- [auth.go](auth/auth.go) - `nil` errors
- [client.go](client/client.go) - Mocking for errors
- [value.go](value/value.go) - Custom JSON marshaling
- [wc.go](wc/wc.go) - Implementing `io.Writer`
- [sha1.go](sha1/sha1.go) - Combining `io.Reader` & `io.Writer`

### Links

- [Knuth vs McIlroy](https://matt-rickard.com/instinct-and-culture) - The power of shell
- [pkg/errors](https://pkg.go.dev/github.com/pkg/errors) - Getting stack trace, using `%+v`
- [When NASA Lost a Spacecraft Due to a Metric Math Mistake](https://www.simscale.com/blog/nasa-mars-climate-orbiter-metric/)
- [go-cleanhttp](https://github.com/hashicorp/go-cleanhttp) - Clean HTTP client
- Interfaces
    - The [io package](https://pkg.go.dev/io)
    - [sort.Interface](https://pkg.go.dev/sort#Interface)
        - Sort [examples](https://pkg.go.dev/sort#pkg-examples)
- Generics
    - [When to use generics](https://go.dev/blog/when-generics)
    - [Generics tutorial](https://go.dev/doc/tutorial/generics)
    - [All your comparable types](https://go.dev/blog/comparable)
- Concurrency
    - [Scheduling in Go](https://www.ardanlabs.com/blog/2018/08/scheduling-in-go-part2.html)
    - [The race detector](https://go.dev/doc/articles/race_detector)
    - [Channel Semantics](https://www.353solutions.com/channel-semantics)
    - [Why are there nil channels in Go?](https://medium.com/justforfunc/why-are-there-nil-channels-in-go-9877cc0b2308)
    - [Amdahl's Law](https://en.wikipedia.org/wiki/Amdahl%27s_law) - Limits of concurrency
    - [Computer Latency at Human Scale](https://twitter.com/jordancurve/status/1108475342468120576/photo/1)
    - [Concurrency is not Parallelism](https://www.youtube.com/watch?v=cN_DpYBzKso) by Rob Pike
    - [errgroup](https://pkg.go.dev/golang.org/x/sync/errgroup#example-Group-JustErrors)
    - [TLA+](https://en.wikipedia.org/wiki/TLA%2B) formal specification for concurrent programs
- Code tools
    - [Generating code](https://go.dev/blog/generate)
    - [staticcheck](https://staticcheck.dev/)
    - [golangci-lint](https://golangci-lint.run/)
    - [gosec](https://github.com/securego/gosec)
    - [govulncheck](https://pkg.go.dev/golang.org/x/vuln/cmd/govulncheck)
    - [golang.org/x/tools/go/analysis](https://pkg.go.dev/golang.org/x/tools/go/analysis)
        - [Using go/analysis to write a custom linter](https://arslan.io/2019/06/13/using-go-analysis-to-write-a-custom-linter/)
    - [stringer](https://pkg.go.dev/golang.org/x/tools/cmd/stringer) - Implement `fmt.Stringer` for your types

### Data & Other

- [catalog.csv](_extra/catalog.csv)
- [Error Cost](_extra/cost.png)

---

## Session 1: Performance Optimization

### Agenda

- Lies, damn lies & benchmarks
- Common benchmark errors
- GC & escape analysis
- Profiling CPU & memory
- Using the trace tool
- Performance hacks


### Code

- [quote](quote/quote.go) - Interfaces escape to heap
- [logs](logs) - Redundant serialization
- [author](author) - Preventing large slices to stay around
- [wc](wc) - Reducing system calls with buffered I/O
- [file](file) - Cost of using interfaces (~2ns on my machine)
- [shop](shop) - Using caching
- [slices](slices/slices.go) - How `append` grows slices
- [httpd](httpd/main.go) - Profiling HTTP servers with custom mux/router
- [tokenizer](tokenizer) - CPU & Memory optimization
- [user_cache](user_cache) - Slice vs Map, benchmarking (size of `n`)

### Links

- [Rules of Optimization Club](https://wiki.c2.com/?RulesOfOptimizationClub)
- [benchstat](https://pkg.go.dev/golang.org/x/perf/cmd/benchstat)
- [Benchmarking Checklist](https://www.brendangregg.com/blog/2018-06-30/benchmarking-checklist.html)
- [Rob Pike's 5 Rules of Programming](https://users.ece.utexas.edu/~adnan/pike.html)
- [Computer Latency at Human Scale](https://twitter.com/jordancurve/status/1108475342468120576)
- [Scheduling in Go](https://www.ardanlabs.com/blog/2018/08/scheduling-in-go-part1.html)
- [A Guide to the Go Garbage Collector](https://go.dev/doc/gc-guide)
- [Garbage Collection In Go](https://www.ardanlabs.com/blog/2018/12/garbage-collection-in-go-part1-semantics.html)
- [Getting Friendly with CPU Caches](https://www.ardanlabs.com/blog/2023/07/getting-friendly-with-cpu-caches.html)
- [Language Mechanics](https://www.ardanlabs.com/blog/2017/05/language-mechanics-on-stacks-and-pointers.html)
- [Tracing Programs with Go](https://www.youtube.com/watch?v=mjixFKO-IdM)
- [High Performance Go Workshop](https://dave.cheney.net/high-performance-go-workshop/gophercon-2019.html)
- [High Performance Networking in Chrome](https://aosabook.org/en/posa/high-performance-networking-in-chrome.html)
- [graphviz](https://graphviz.org/)
- [Profile-guided optimization](https://go.dev/doc/pgo)


### Data & Other

- [Complexity](_extra/complexity.png)
- [CPU Cache](_extra/cpu-cache.png)
- [Stack](_extra/stack.png)
- [Miki's Optimization Guide](_extra/optimize.md)
