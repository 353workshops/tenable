package main

import "fmt"

func main() {
	var s []int
	currCap := 0
	for i := 0; i < 10_000; i++ {
		s = append(s, i)
		if c := cap(s); c != currCap {
			fmt.Println(currCap, "->", c)
			currCap = c
		}
	}
}
