package wc

import (
	"io"
)

func readByte(r io.Reader) (byte, error) {
	var buf [1]byte
	if _, err := r.Read(buf[:]); err != nil {
		return 0, err
	}

	return buf[0], nil
}

// 611511
// 20677728

/*
func LineCount(r io.Reader) (int, error) {
	r = bufio.NewReader(r)
	n := 0
	for {
		b, err := readByte(r)
		if errors.Is(err, io.EOF) {
			break
		}

		if err != nil {
			return 0, err
		}

		if b == '\n' {
			n++
		}

	}

	return n, nil
}
*/

type lc int

// Write implement io.Writer
func (l *lc) Write(data []byte) (int, error) {
	for _, b := range data {
		if b == '\n' {
			*l++
		}
	}

	return len(data), nil
}

func LineCount(r io.Reader) (int, error) {
	var l lc
	if _, err := io.Copy(&l, r); err != nil {
		return 0, err
	}

	return int(l), nil
}
