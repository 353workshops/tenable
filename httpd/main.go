package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"os"
	"perf/tokenizer"

	"net/http/pprof"
	_ "net/http/pprof"
)

func tokenizeHandler(w http.ResponseWriter, r *http.Request) {
	data, err := io.ReadAll(http.MaxBytesReader(w, r.Body, 1_000_000))
	if err != nil {
		http.Error(w, "can't read", http.StatusBadRequest)
		return
	}

	text := string(data)
	tokens := tokenizer.Tokenize(text)

	reply := map[string]any{
		"tokens": tokens,
	}

	if err := json.NewEncoder(w).Encode(reply); err != nil {
		slog.Warn("can't encode", "error", err)
	}
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/tokenize", tokenizeHandler)
	mux.HandleFunc("/debug/pprof/profile", pprof.Profile)

	srv := http.Server{
		Addr:    ":8080",
		Handler: mux,
	}

	slog.Info("starting", "address", srv.Addr)
	if err := srv.ListenAndServe(); err != nil {
		fmt.Fprintf(os.Stderr, "error: can't listen on %s", srv.Addr)
		os.Exit(1)
	}
}
