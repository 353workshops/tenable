package main

//go:generate go run gen_db.go
//go:generate go fmt db.go

import (
	"encoding/json"
	"fmt"
	"log/slog"
	"net/http"
	"os"
)

func itemHandler(w http.ResponseWriter, r *http.Request) {
	sku := r.PathValue("sku")
	if sku == "" {
		http.Error(w, "missing sku", http.StatusBadRequest)
		return
	}

	item, ok := db[sku]
	if !ok {
		http.Error(w, "unknown sku", http.StatusNotFound)
		return
	}

	if err := json.NewEncoder(w).Encode(item); err != nil {
		slog.Error("itemHandler: encode", "error", err)
	}
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("GET /item/{sku}", itemHandler)

	addr := ":8080"
	srv := http.Server{
		Addr:    addr,
		Handler: mux,
	}

	slog.Info("server starting", "address", addr)
	if err := srv.ListenAndServe(); err != nil {
		fmt.Fprintf(os.Stderr, "error: %s\n", err)
		os.Exit(1)
	}
}
