module perf

go 1.22

require (
	github.com/hashicorp/golang-lru/v2 v2.0.7
	github.com/stretchr/testify v1.8.4
	go.etcd.io/bbolt v1.3.8
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
