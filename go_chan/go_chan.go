package main

import (
	"context"
	"fmt"
	"net/http"
	"time"
)

func main() {
	go fmt.Println("goroutine")
	fmt.Println("main")

	time.Sleep(time.Millisecond)

	ch := make(chan string)
	go func() {
		ch <- "hi" // send
	}()
	val := <-ch // receive
	fmt.Println("val:", val)

	go func() {
		defer close(ch)
		for i := range 3 {
			ch <- fmt.Sprintf("message #%d", i)
		}
	}()

	for v := range ch {
		fmt.Println("range:", v)
	}

	/* The above range does
	for {
		v, ok := <- ch
		if !ok {
			break
		}
		fmt.Println("range:", v)
	}
	*/

	// ch is closed
	v := <-ch
	fmt.Printf("v: %#v\n", v)

	// zero vs missing
	v, ok := <-ch
	fmt.Printf("v: %#v (ok=%v)\n", v, ok)

	// ch <- "hi" // send to closed channel - PANIC

	// buffered channel to avoid goroutine leak
	ch1, ch2 := make(chan int, 1), make(chan int, 1)
	go func() {
		time.Sleep(100 * time.Millisecond)
		ch1 <- 1
	}()
	go func() {
		time.Sleep(200 * time.Millisecond)
		ch2 <- 2
	}()

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Millisecond)
	defer cancel()
	select {
	case v := <-ch1:
		fmt.Println("ch1:", v)
	case v := <-ch2:
		fmt.Println("ch2:", v)
	// case <-time.After(10 * time.Millisecond):
	case <-ctx.Done():
		fmt.Println("timeout")
	}

	ch3 := Future(sleeper)
	s := <-ch3
	fmt.Println("s:", s)

	fanOut()
}

func fanOut() {
	urls := []string{
		"https://tenable.com/ot",
		"https://go.dev",
		"https://examp.com",
	}

	type result struct {
		url    string
		status int
		err    error
	}

	pool := make(chan bool, 2) // limit to 2
	ch := make(chan result)
	for _, url := range urls {
		// Go < 1.22: url := url
		go func() {
			pool <- true
			defer func() {
				<-pool
			}()
			r := result{url: url}
			resp, err := http.Get(url)
			if err != nil {
				r.err = err
			} else {
				r.status = resp.StatusCode
			}
			ch <- r
		}()
	}

	for range urls {
		r := <-ch
		fmt.Println(r)
	}

}

func sleeper() int {
	time.Sleep(100 * time.Millisecond)
	return 7
}

// wait for result, defer, future, promise ...
func Future[T any](fn func() T) <-chan T {
	ch := make(chan T, 1)
	go func() {
		ch <- fn()
	}()

	return ch
}

/* goroutine communication
- Wait for end: sync.WaitGroup
- Return/send value: channel
- Cancel: context
*/

/*
Channel semantics:
- send/receive will block until opposite operation
	- Buffer channel of size "n" has "n" non blocking sends
- receive from closed channel will get zero value without blocking
- send/close to closed channel will panic
- send/receive from a nil channel block forever
*/

// Two types of problems:
// synchronization: access to shared resource (sync.Mutex ...)
// coordination: wait for goroutine to finish ... (channel, sync.WaitGroup)
