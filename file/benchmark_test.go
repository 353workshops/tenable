package file

import (
	"io"
	"testing"
)

var (
	f             = &File{}
	r   io.Reader = f
	buf           = make([]byte, 10)
)

func BenchmarkMethod(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, err := f.Read(buf)
		if err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkIface(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, err := r.Read(buf)
		if err != nil {
			b.Fatal(err)
		}
	}
}
