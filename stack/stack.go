package stack

import "errors"

type node[T any] struct {
	value T
	next  *node[T]
}

type Stack[T any] struct {
	head *node[T]
}

func (s *Stack[T]) Push(v T) {
	s.head = &node[T]{v, s.head}

}

func (s *Stack[T]) Len() int {
	size := 0
	for n := s.head; n != nil; n = n.next {
		size++
	}

	return size
}

var ErrEmpty = errors.New("empty stack")

func (s *Stack[T]) Pop() (T, error) {
	if s.head == nil {
		var zero T
		return zero, ErrEmpty
	}

	n := s.head
	s.head = s.head.next
	return n.value, nil
}

/* Won't compile
func (s Stack[T])[V any](v V) {
}
*/

// type Cache[K comparable, V any] map[K]V
