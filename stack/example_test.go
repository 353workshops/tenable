package stack

import "fmt"

func ExampleStack() {
	var s Stack[int]
	s.Push(1)
	s.Push(2)
	s.Push(3)

	fmt.Println(s.Pop())
	fmt.Println(s.Pop())
	fmt.Println(s.Pop())
	fmt.Println(s.Pop())

	// Output:
	// 3 <nil>
	// 2 <nil>
	// 1 <nil>
	// 0 empty stack
}
