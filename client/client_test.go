package client

import (
	"fmt"
	"log/slog"
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"
)

type mockTripper struct{}

func (m mockTripper) RoundTrip(*http.Request) (*http.Response, error) {
	slog.Info("mockTripper was here")
	return nil, fmt.Errorf("fake")
}

func TestClient_HealthError(t *testing.T) {
	c := APIClient{
		baseURL: "https://tenable.com",
	}
	c.c.Transport = mockTripper{}

	err := c.Health()
	require.Error(t, err)
}
