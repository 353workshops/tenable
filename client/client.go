package client

import (
	"fmt"
	"net/http"
)

type APIClient struct {
	c       http.Client
	baseURL string
}

func (c *APIClient) Health() error {
	url := fmt.Sprintf("%s/health", c.baseURL)
	resp, err := c.c.Get(url)

	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf(resp.Status)
	}
	return nil
}
