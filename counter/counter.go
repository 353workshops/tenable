package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	counter := 0

	const ng = 10
	var wg sync.WaitGroup
	var mu sync.Mutex
	wg.Add(ng)
	for range ng {
		go func() {
			defer wg.Done()
			for range 1000 {

				// See also atomic.AddInt64
				mu.Lock()
				{
					counter++
				}
				mu.Unlock()

				time.Sleep(time.Microsecond)
			}
		}()
	}
	wg.Wait()
	fmt.Println("counter:", counter)
}

// go run -race counter.go
// go build -race
// go test -race
