package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"os"
	"time"
)

func main() {
	r := Writer
	fmt.Println("r:", r)       // writer
	fmt.Printf("d: %d\n", r)   // 2
	fmt.Printf(" v: %v\n", r)  // writer
	fmt.Printf("+v: %+v\n", r) // writer
	fmt.Printf("#v: %#v\n", r) // 0x2
	// To support %#v implement GoStringer
	// For finer control, implement fmt.Formatter

	json.NewEncoder(os.Stdout).Encode(r)
	xml.NewEncoder(os.Stdout).Encode(r)

	fmt.Println()
	u := User{19, "elliot"}
	fmt.Printf(" v: %v\n", u)
	fmt.Printf("+v: %+v\n", u)
	fmt.Printf("#v: %#v\n", u)

	if err := Login("daffy", "rabbitseason"); err != nil {
		fmt.Println("ERROR:", err)
	}

}

func Login(user, passwd string) error {
	// var err *AuthError // BUG: non-nil error
	/* iface{
		data: err // nil
		tab: &AuthError // not nil
	}
	*/
	var err error
	/* iface{
		data: nil
		tab: nil
	}
	*/

	/*
		err = &AuthError{
			Time:   time.Now(),
			User:   "joe",
			Reason: "password mismatch",
		}
	*/

	return err // TODO
}

func (a *AuthError) Error() string {
	return a.Reason

}

type AuthError struct {
	Time   time.Time
	User   string
	Reason string
}

func (r Role) MarshalText() ([]byte, error) {
	return []byte(r.String()), nil
}

/* What fmt.Print* does
func Print(v any) {
	if s, ok := v.(fmt.Stringer); ok {
		print(s)
	}
	// ...
}
*/

type User struct {
	ID    int
	Login string
}

// String implement fmt.Stringer
func (r Role) String() string {
	switch r {
	case Reader:
		return "reader"
	case Writer:
		return "writer"
	case Admin:
		return "admin"
	}

	return fmt.Sprintf("<Role %d>", r)
}

const (
	Reader Role = iota + 1
	Writer
	Admin
)

type Role byte

// Other interfaces that change behavior
// - json.Marshaler, json.Unmarshaler
// - encoding.TextMarshaler, encode.TextUnmarshaler (json, CSV)
