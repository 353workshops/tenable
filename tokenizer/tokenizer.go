package tokenizer

import (
	"strings"
)

var (
	suffixes = []string{"ed", "ing", "s"}
)

// works -> work, worked -> work, working -> work
func Stem(word string) string {
	for _, s := range suffixes {
		if strings.HasSuffix(word, s) {
			return word[:len(word)-len(s)]
		}
	}

	return word
}

// Who's on first? -> [who on first]
func Tokenize(text string) []string {
	words := initialSplit(text)
	tokens := make([]string, 0, len(words))
	for _, w := range words {
		tok := strings.ToLower(w)
		tok = Stem(tok)
		if tok != "" {
			tokens = append(tokens, tok)
		}
	}
	return tokens
}

func isLetter(b byte) bool {
	return (b >= 'a' && b <= 'z') || (b >= 'A' && b <= 'Z')
}

func initialSplit(text string) []string {
	const avgTokens = 100 // Median tokens in messages (2023-12-18)
	fs := make([]string, 0, avgTokens)

	i := 0
	for i < len(text) {
		// eat start
		for i < len(text) && !isLetter(text[i]) {
			i++
		}
		if i == len(text) {
			break
		}

		j := i + 1
		for j < len(text) && isLetter(text[j]) {
			j++
		}

		fs = append(fs, text[i:j])
		i = j
	}

	return fs

}
