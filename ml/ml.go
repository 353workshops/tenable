package ml

import (
	"fmt"
)

type Number interface {
	~int | ~float64
}

type Int int

// Go generics implementation uses "gc shape stenciling"
// Two types are in the same shape if the underlying type has the same shape
// All pointers have the same GC shape

// T is a type constraints, not a new type
func Relu[T Number](n T) T {
	if n < 0 {
		return 0
	}

	return n
}

type Ordered interface {
	~int | ~float64 | ~string
}

func Max[T Ordered](values []T) (T, error) {
	if len(values) == 0 {
		/*
			var zero T
			return zero, fmt.Errorf("Max of empty slice")
		*/
		return zero[T](), fmt.Errorf("Max of empty slice")
	}

	m := values[0]
	for _, v := range values[1:] {
		if v > m {
			m = v
		}
	}
	return m, nil
}

func zero[T any]() (v T) {
	return
}
