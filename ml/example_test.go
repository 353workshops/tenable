package ml

import "fmt"

func Example() {
	fmt.Println(Relu(-3))
	fmt.Println(Relu(3))
	fmt.Println(Relu(3.0))
	fmt.Println(Relu(Int(3)))

	fmt.Println(Max([]int{2, 3, 1}))
	fmt.Println(Max([]float64{2, 3, 1}))
	fmt.Println(Max[int](nil))

	// Output:
	// 0
	// 3
	// 3
	// 3
	// 3 <nil>
	// 3 <nil>
	// 0 Max of empty slice
}
